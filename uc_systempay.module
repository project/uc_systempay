<?php

/**
 * @file
 * Integrates the Systempay payment service with Ubercart.
 * Developped by anrikun Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
 * http://www.absyx.fr
 */



/*******************************************************************************
 * Hook Functions (Drupal)
 ******************************************************************************/

/**
 * Implementation of hook_menu().
 */
function uc_systempay_menu() {
  $items['cart/systempay/response'] = array(
    'title' => 'Systempay response',
    'page callback' => 'uc_systempay_response',
    'access callback' => 'uc_systempay_response_access',
    'type' => MENU_CALLBACK,
  );
  $items['cart/systempay/success'] = array(
    'title' => 'Systempay payment success',
    'page callback' => 'uc_systempay_success',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['cart/systempay/cancel'] = array(
    'title' => 'Systempay payment cancelled',
    'page callback' => 'uc_systempay_cancel',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['cart/systempay/return'] = array(
    'title' => 'Systempay payment return',
    'page callback' => 'uc_systempay_return',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

// Make sure Systempay always has access to send responses.
function uc_systempay_response_access() {
  return TRUE;
}



/**
 * Implementation of hook_menu_alter().
 */
function uc_systempay_menu_alter(&$items) {
  $callback = $items['cart/checkout/complete']['page callback'];
  if ($callback != 'uc_systempay_checkout_complete') {
    variable_set('uc_systempay_checkout_complete_bak', $callback);
    $items['cart/checkout/complete']['page callback'] = 'uc_systempay_checkout_complete';
  }
}

// Override uc_cart_checkout_complete() when payment method is systempay.
function uc_systempay_checkout_complete() {
  if (!$_SESSION['do_complete']
   || !($order = uc_order_load(intval($_SESSION['cart_order'])))
   || $order->payment_method != 'systempay'
   || $order->uid == 0) {
    return call_user_func(variable_get('uc_systempay_checkout_complete_bak', 'uc_cart_checkout_complete'));
  }

  // Get checkout_complete output.
  global $user;
  if ($order->uid == $user->uid) {

    // Build output for the already logged in user.
    $messages['uc_msg_order_submit_format'] = variable_get('uc_msg_order_submit', uc_get_message('completion_message'));
    $messages['uc_msg_order_logged_in_format'] = variable_get('uc_msg_order_logged_in', uc_get_message('completion_logged_in'));
    $messages['uc_msg_continue_shopping_format'] = variable_get('uc_msg_continue_shopping', uc_get_message('continue_shopping'));

    $output_message = '';
    foreach ($messages as $format => $message) {
      $message = token_replace_multiple($message, array('global' => NULL, 'order' => $order));
      $message = check_markup($message, variable_get($format, FILTER_FORMAT_DEFAULT), FALSE);
      $output_message .= $message;
    }
    $output = theme('uc_cart_complete_sale', $output_message);
  }
  else {

    // Use the output built during response.
    $output = $order->data['systempay']['complete_sale_output'];
    unset($order->data['systempay']['complete_sale_output']);
    uc_systempay_save_order_data($order);

    // Login the user if specified.
    if (!$user->uid && variable_get('uc_new_customer_login', FALSE)) {
      user_external_login(user_load($order->uid));
    }
  }

  // Clear cart data.
  uc_cart_empty(uc_cart_get_id());
  unset($_SESSION['cart_order'], $_SESSION['do_complete']);

  // Redirect to a custom page if defined.
  $page = variable_get('uc_cart_checkout_complete_page', '');
  if (!empty($page)) {
    drupal_goto($page);
  }

  return $output;
}



/**
 * Implementation of hook_form_FORM_ID_alter(); alter uc_cart_checkout_review_form.
 */
function uc_systempay_form_uc_cart_checkout_review_form_alter(&$form, &$form_state) {
  if (($order_id = intval($_SESSION['cart_order']))
   && ($order = uc_order_load($order_id))
   && $order->payment_method == 'systempay') {
    unset($form['submit']);
    $form['#prefix'] = '<table style="display: inline; padding-top: 1em;"><tr><td>';
    $form['#suffix'] = '</td><td>'. drupal_get_form('uc_systempay_form', $order) .'</td></tr></table>';
  }
}



/*******************************************************************************
 * Hook Functions (Ubercart)
 ******************************************************************************/

/**
 * Implementation of hook_payment_method().
 */
function uc_systempay_payment_method() {
  $title = variable_get('uc_systempay_method_title', t('Credit card'));

  $methods[] = array(
    'id' => 'systempay',
    'name' => t('Systempay'),
    'title' => $title,
    'review' => t('Credit card'),
    'desc' => t('Redirect users to submit payments through Systempay.'),
    'callback' => 'uc_payment_method_systempay',
    'weight' => 3,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}



/*******************************************************************************
 * Callback Functions, Forms, and Tables
 ******************************************************************************/

/**
 * Add Systempay settings to the payment method settings form.
 */
function uc_payment_method_systempay($op, &$arg1) {
  switch ($op) {
    case 'cart-details':
      $details = variable_get('uc_systempay_method_description', '');
      return $details;

    case 'settings':
      $form['uc_systempay_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Systempay URL'),
        '#description' => t('The URL to the Systempay payment platform.'),
        '#default_value' => variable_get('uc_systempay_url', 'https://systempay.cyberpluspaiement.com/vads-payment/'),
      );
      $form['uc_systempay_site_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Site ID'),
        '#description' => t('The ID your site was assigned when subscribing to Systempay.'),
        '#default_value' => variable_get('uc_systempay_site_id', ''),
        '#size' => 10,
        '#maxlength' => 8,
      );
      $form['uc_systempay_certificate'] = array(
        '#type' => 'textfield',
        '#title' => t('Certificate'),
        '#description' => t('The certificate your site was assigned when subscribing to Systempay.'),
        '#default_value' => variable_get('uc_systempay_certificate', ''),
        '#size' => 20,
        '#maxlength' => 16,
      );
      $form['uc_systempay_ctx_mode'] = array(
        '#type' => 'radios',
        '#title' => t('Running mode'),
        '#options' => array(t('Test'), t('Production')),
        '#default_value' => variable_get('uc_systempay_ctx_mode', 0),
      );
      $form['uc_systempay_server_url'] = array(
        '#type' => 'item',
        '#title' => t('Server URL'),
        '#value' => url('cart/systempay/response', array('absolute' => TRUE)),
        '#description' => 'This URL has to be set through your bank administration interface for Systempay to function properly.',
      );

      return $form;
  }
}



/**
 * Form to build the submission to Systempay.
 */
function uc_systempay_form($form_state, $order) {
  $fields = array(
    'order_id' => $order->order_id,
    'amount' => round($order->order_total * 100, 0),
    'capture_delay' => '',
    'currency' => uc_systempay_currency(variable_get('uc_currency_code', 'USD')),
    'cust_email' => $order->primary_email,
    'ctx_mode' => (variable_get('uc_systempay_ctx_mode', 0) == 1) ? 'PRODUCTION' : 'TEST',
    'payment_cards' => '',
    'payment_config' => 'SINGLE',
    'return_mode' => 'GET',
    'site_id' => variable_get('uc_systempay_site_id', ''),
    'trans_date' => gmdate('YmdHis', time()),
    'trans_id' => rand(0, 8) . substr(time(), -5),
    'validation_mode' => '',
    'version' => 'V1',
    'url_success' => url('cart/systempay/success', array('absolute' => TRUE)),
    'url_cancel' => url('cart/systempay/cancel', array('absolute' => TRUE)),
    'url_return' => url('cart/systempay/return', array('absolute' => TRUE)),
  );
  $fields['signature'] = uc_systempay_signature($fields);

  // Add available address fields.
  $map = array(
    'billing_phone' => 'cust_phone',
    'billing_last_name' => 'cust_name',
    'billing_first_name' => 'cust_name',
    'billing_street1' => 'cust_address',
    'billing_street2' => 'cust_address',
    'billing_city' => 'cust_city',
    'billing_postal_code' => 'cust_zip',
  );
  foreach ($map as $order_key => $field_key) {
    if (isset($order->$order_key) && strlen($order->$order_key)) {
      $fields[$field_key] = isset($fields[$field_key]) ? $fields[$field_key] .', '. $order->$order_key : $order->$order_key;
    }
  }
  if (isset($order->billing_country)) {
    $country_data = uc_get_country_data(array('country_id' => $order->billing_country));
    if ($country_data) {
      $fields['cust_country'] = $country_data[0]['country_iso_code_2'];
    }
  }

  // Add trans_date and trans_id to the order's data array.
  $order->data['systempay']['trans_date'] = $fields['trans_date'];
  $order->data['systempay']['trans_id'] = $fields['trans_id'];

  // Save the updated data array to the database.
  uc_systempay_save_order_data($order);

  $form['#action'] = variable_get('uc_systempay_url', 'https://systempay.cyberpluspaiement.com/vads-payment/');

  foreach ($fields as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit order'),
  );

  return $form;
}



/**
 * Save order's updated data array to the database.
 */
function uc_systempay_save_order_data($order) {
  db_query("UPDATE {uc_orders} SET data = '%s' WHERE order_id = %d", serialize($order->data), $order->order_id);
}



/**
 * Handle an incoming payment.
 */
function uc_systempay_response() {
  if (empty($_POST)) {
    watchdog('uc_systempay', 'Response attempted without any POST data.', array(), WATCHDOG_WARNING);
    return;
  }

  $order = uc_systempay_validate_post(TRUE);
  if (!$order) {
    watchdog('uc_systempay', 'Invalid response.', array(), WATCHDOG_ERROR);
    return;
  }
  $order_id = $order->order_id;

  if ($_POST['auth_result'] != '00') {
    uc_order_comment_save($order_id, 0, t("The customer's attempted payment from a bank account may have failed. Authorisation result is @result.", array('@result' => $_POST['auth_result'])), 'admin');
    return;
  }

  $order->data['systempay']['complete_sale_output'] = uc_cart_complete_sale($order);
  uc_systempay_save_order_data($order);

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'amount',
  );
  $options = array(
    'sign' => FALSE,
  );
  $amount = $_POST['amount'] / 100;
  $currency = uc_systempay_currency($_POST['currency'], TRUE);

  $comment = t('Systempay authorisation #: @number', array('@number' => $_POST['auth_number']));
  uc_payment_enter($order_id, 'systempay', $amount, $order->uid, $_POST, $comment);

  uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through Systempay.', array('@amount' => uc_price($amount, $context, $options), '@currency' => $currency)), 'order', 'payment_received');
  uc_order_comment_save($order_id, 0, t('Systempay response reported a payment of @amount @currency.', array('@amount' => uc_price($amount, $context, $options), '@currency' => $currency)));
}



/**
 * Handle a successful payment.
 */
function uc_systempay_success() {
  $order = uc_systempay_validate_post();
  if (!$order) {
    drupal_goto('cart');
  }

  // Leave an ambiguous message when posted order ID is different from the one
  // in user's session.
  if (intval($_SESSION['cart_order']) != $order->order_id) {
    drupal_set_message(t('Thank you for your order! We will be notified by Systempay that we have received your payment.'));
    drupal_goto('cart');
  }

  // This lets us know it's a legitimate access of the complete page.
  $_SESSION['do_complete'] = TRUE;

  drupal_goto('cart/checkout/complete');
}



/**
 * Handle a cancelled payment.
 */
function uc_systempay_cancel() {
  unset($_SESSION['cart_order']);

  drupal_set_message(t('Your payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));

  drupal_goto('cart');
}



/**
 * Handle an unsuccessful payment.
 */
function uc_systempay_return() {
  unset($_SESSION['cart_order']);

  drupal_set_message(t('Sorry, your payment could not be processed. Please try again or contact us for assistance.'));

  drupal_goto('cart');
}



/**
 * 
 */
function uc_systempay_signature($fields = NULL) {
  if (is_array($fields)) {
    $keys = array('version', 'site_id', 'ctx_mode', 'trans_id', 'trans_date', 'validation_mode', 'capture_delay', 'payment_config',
      'payment_cards', 'amount', 'currency');
  }
  else {
    $fields = $_REQUEST;
    $keys = array('version', 'site_id', 'ctx_mode', 'trans_id', 'trans_date', 'validation_mode', 'capture_delay', 'payment_config',
      'card_brand', 'card_number', 'amount', 'currency', 'auth_mode', 'auth_result', 'auth_number', 'warranty_result', 'payment_certificate', 'result');
    if (isset($fields['hash'])) {
      $keys[] = 'hash';
    }
  }

  $values = array();
  foreach ($keys as $key) {
    $values[] = $fields[$key];
  }
  $values[] = variable_get('uc_systempay_certificate', '');
  return sha1(implode('+', $values));
}



/**
 * Validate data posted by Systempay.
 *
 * @param $check_hash
 *   TRUE if hash field has to be present in posted data.
 * @return
 *   The order matching posted data, FALSE otherwise.
 */
function uc_systempay_validate_post($check_hash = FALSE) {
  if ($check_hash && !isset($_REQUEST['hash'])) {
    return FALSE;
  }
  if (!isset($_REQUEST['signature']) || ($_REQUEST['signature'] != uc_systempay_signature())) {
    return FALSE;
  }
  if (isset($_REQUEST['order_id']) && ($order = uc_order_load(intval($_REQUEST['order_id'])))) {
    if (!isset($order->data['systempay']['trans_date']) || !isset($order->data['systempay']['trans_id'])) {
      return FALSE;
    }
    if ($_REQUEST['amount'] == round($order->order_total * 100, 0)
     && $_REQUEST['trans_date'] == $order->data['systempay']['trans_date']
     && $_REQUEST['trans_id'] == $order->data['systempay']['trans_id']) {
      return $order;
    }
  }
  return FALSE;
}



/**
 * Get the currency code of the provided store currency.
 * If $flip is TRUE, return the store currency of the provided currency code.
 */
function uc_systempay_currency($currency, $flip = FALSE) {
  $currencies = array(
    'EUR' => '978',
    'USD' => '840',
    'CHF' => '756',
    'GBP' => '826',
    'CAD' => '124',
    'JPY' => '392',
    'MXP' => '484',
    'TRL' => '792',
    'AUD' => '036',
    'NZD' => '554',
    'NOK' => '578',
    'BRC' => '986',
    'ARP' => '032',
    'KHR' => '116',
    'TWD' => '901',
    'SEK' => '752',
    'DKK' => '208',
    'KRW' => '410',
    'SGD' => '702',
  );
  if ($flip) {
    $currencies = array_flip($currencies);
  }
  return isset($currencies[$currency]) ? $currencies[$currency] : '';
}
